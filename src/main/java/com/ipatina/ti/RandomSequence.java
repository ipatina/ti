package com.ipatina.ti;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomSequence {

    private Random random;

    public RandomSequence() {
        random = new Random();
    }

    public String generate(Map<String, Integer> probabilities, int length) {
        if (checkInput(probabilities)) {
            throw new IllegalArgumentException("Input probability not equal to 100");
        }

        List<String> chars = probabilities.keySet().stream().collect(Collectors.toList());

        StringBuilder result = new StringBuilder();

        List<Integer> shouldBeInSequence = new ArrayList<>();
        for (Integer i : probabilities.values()) {
            int p = length * i;
            shouldBeInSequence.add(p);
        }

        // reduce
        int sumOfProbs = 0;
        for (int i : shouldBeInSequence) {
            sumOfProbs += i;
        }

        if (sumOfProbs < length) {
            int remainder = length - sumOfProbs;
            while (remainder-- > 0) {
                int r = random.nextInt(chars.size());
                int i = shouldBeInSequence.get(r);
                shouldBeInSequence.set(r, ++i);
            }
        }

        List<Integer> repeats = new ArrayList<>();
        for (Integer i : shouldBeInSequence) {
            repeats.add(0);
        }

        while (result.length() != length) {
            int p = random.nextInt(chars.size());
            if (repeats.get(p) < shouldBeInSequence.get(p)) {
                result.append(chars.get(p));
                int i = repeats.get(p);
                repeats.set(p, ++i);
            }
        }

        return result.toString();
    }

    private boolean checkInput(Map<String, Integer> probabilities) {
        int sum = 0;
        for (Integer i : probabilities.values()) {
            sum += i;
        }

        return sum != 100;
    }
}
