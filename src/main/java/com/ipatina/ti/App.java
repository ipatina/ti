package com.ipatina.ti;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class App {

    private final static Logger LOG = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) { // prob table and length

        if (args.length < 2) {
            throw new RuntimeException("Missing arguments");
        }

        Map<String, Integer> probabilities = new HashMap<>();
        RandomSequence randomSequence = new RandomSequence();

        try {
            Reader in = new FileReader(args[0]);
            Iterable<CSVRecord> records = CSVFormat.newFormat(',').parse(in);

            records.forEach(record -> {
                int probability = Integer.parseInt(record.get(1));
                String letter = record.get(0);
                probabilities.put(letter, probability);
            });
        } catch (FileNotFoundException e) {
            LOG.error("File not found", e);
        } catch (IOException e) {
            LOG.error("IOException occured while reading", e);
        }

        try (FileWriter writer = new FileWriter("result.txt")) {
            int length = Integer.parseInt(args[1]);
            String result = randomSequence.generate(probabilities, length);
            writer.write(result);
        } catch (IOException e) {
            LOG.error("IOException occured while writing", e);
        }
    }
}
