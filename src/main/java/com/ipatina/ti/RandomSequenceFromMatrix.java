package com.ipatina.ti;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class RandomSequenceFromMatrix {

    private Random random;
    private StringBuilder result;
    private Map<String, List<Integer>> probabilities;
    private int length;

    public RandomSequenceFromMatrix(Map<String, List<Integer>> probabilities, int length) {
        random = new Random();
        result = new StringBuilder();
        this.probabilities = probabilities;
        this.length = length;
    }

    public String generate() {
        if (checkInput()) {
            throw new IllegalArgumentException("Input probability not equal to 100");
        }

        int nextLetter, nextProb;
        while (result.length() != length) {
            nextLetter = random.nextInt(8);

        }


        return result.toString();
    }

    private void next(String letter, int counter) {
        if (counter == length) return;

    }

    private boolean checkInput() {
        int sum;
        for (List<Integer> list : probabilities.values()) {
            sum = 0;
            for (Integer i : list) {
                sum += i;
            }
            if (sum != 100) return false;
        }

        return true;
    }
}
