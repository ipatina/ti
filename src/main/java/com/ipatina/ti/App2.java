package com.ipatina.ti;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App2 {

    private final static Logger LOG = LoggerFactory.getLogger(App2.class);

    public static void main(String[] args) {

        if (args.length < 2) {
            throw new RuntimeException("Missing arguments");
        }

        Map<String, List<Integer>> probabilities = new HashMap<>();
        RandomSequenceFromMatrix randomSequence =
                new RandomSequenceFromMatrix(probabilities, Integer.parseInt(args[1]));

        try {
            Reader in = new FileReader(args[0]);
            Iterable<CSVRecord> records = CSVFormat.newFormat(' ').parse(in);

            records.forEach(record -> {
//                int probability = Integer.parseInt(record.get(1));
                List<Integer> values = new ArrayList<>();
                record.forEach(value -> {
                    if (!value.matches("\\w")) {
                        values.add(Integer.parseInt(value));
                    }
                });
                String letter = record.get(0);
                probabilities.put(letter, values);
            });

            String s = "";
        } catch (FileNotFoundException e) {
            LOG.error("File not found", e);
        } catch (IOException e) {
            LOG.error("IOException occured while reading", e);
        }
    }
}
